#!/bin/bash

declare _archivo='./suicidios_final.csv'

declare -a paises=$(
    awk -F , 'NR!=1{paises[$1]=$2}END{for (pais in paises) print pais}' "$_archivo"
)

declare -A anio_por_pais

for pais in ${paises[@]}
do
    info_por_pais[$pais]=$(awk -F , -v pais="$pais" '$1==pais{print $2}' "$_archivo")
done