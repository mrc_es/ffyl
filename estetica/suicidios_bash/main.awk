#!/usr/bin/awk

# 1. Country
# 2. Year
# 3. Sex
# 4. Age
# 1. Suicides No

function walk_array(arr, name, i)
{
    for (i in arr) {
        if (isarray(arr[i]))
            walk_array(arr[i], (name "[" i "]"))
        else
            printf("%s[%s] = %s\n", name, i, arr[i])
    }
}

NR!=1{
	reporte[$1][$2][$3]+=$5
}
END {
	
	walk_array(reporte,"Reporte: ")
}