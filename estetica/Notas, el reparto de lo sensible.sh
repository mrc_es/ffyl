: << __conceptos
 
 Reparto de lo sensible (RS): ese sistema de evidencias sensibles 
                              que al mismo tiempo hace visible:
                                - la existencia de un común 
                                - y los recortes que allí definen los lugares y las partes respectivas.

 Estética (E): - no una teoría del arte que remitiría a los efectos sobre la sensibilidad, 
               - sino un régimen específico de identificación y de pensamiento de las artes: 
                   - un modo de articulación entre:
                       - maneras de hacer, 
                       - formas de visibilidad de esas maneras de hacer,
                       - modos de pensabilidad de sus relaciones, 
                         que implican una cierta idea de la efectividad del pensamiento.
 
 Esta "estética" se puede entender como el sistema de formas a priori que determinan lo que se da a sentir.

 Modernidad (M): - un intento desesperado de crear un “ámbito propio del arte”, 
                   vinculándolo a una teleología final simple de la evolución y ruptura históricas

 Ficción (F): Reordenamientos materiales de signos e imágenes, 
              de las relaciones entre lo que se ve y lo que se dice, 
              entre lo que se hace y lo que se puede hacer.
__conceptos

: << __1_reparto
 -------------------------------------------------
 
  1. Del reparto de lo sensible y de las
     relaciones que establece entre la
     política y la estética
 
 -------------------------------------------------
 
 (E) Estética: no una teoría del arte que remitiría a los efectos sobre la sensibilidad, 
               sino un régimen específico de identificación y de pensamiento de las artes: 
                un modo de articulación entre
                  maneras de hacer, 
                  formas de visibilidad de esas maneras de hacer 
                  y modos de pensabilidad de sus relaciones, 
                que implican una cierta idea de la efectividad del pensamiento.
 
 Reparto de lo sensible (RS): ese sistema de evidencias sensibles 
                              que al mismo tiempo hace visible la existencia de un común 
                              y los recortes que allí definen los lugares y las partes respectivas.
 
 RS: Es el lugar donde se juega la política, a una cierta estética política
 RS: Hace ver quién puede tener parte en lo común en función de lo que hace, 
     del tiempo y el espacio en los cuales esta actividad se ejerce.
       |
       |_ Tener tal o cual "ocupación" define competencias o incompetencias 
          respecto a lo común
            |
            |_ Eso define el hecho de ser o no visible en un espacio común,
               dotado de una palabra común, etc.
 RS: fija, al mismo tiempo, un común repartido y partes exclusivas.
 
 Esta repartición de partes y de lugares se funda en un reparto de:
    - espacios,
    - tiempos 
    - formas de actividad
 |
 |_ que determina la manera misma en que un común se ofrece a la participación 
    y donde los unos y los otros tienen parte en este reparto.
 
 (Platón) |Los artesanos| no pueden ocuparse de cosas comunes porque "no tienen tiempo"
                |         de dedicarse a otra cosa que su trabajo.
                |
 No pueden estar en "otro sitio" porque "el trabajo no espera".
 
 La "ocupación" define competencias o incompetencias respecto a lo común.
        |_ Eso define el hecho de ser o no visible en un espacio común, 
           dotado de una palabra común, etc
 
 Entonces, hay, en la base de la política, una "estética" que no tiene nada que ver con
 "la estetización de la política" propia de la "época de masas".
   |
 Esta "estética" se puede entender como el sistema de formas a priori que determinan lo que se da a sentir.
       |
   Es un recorte de:
     - tiempos y de espacio, 
     - de lo visible y de lo invisible, 
     - de la palabra y del ruido que define a la vez el lugar 
        y la problemática de la política como forma de experiencia
                                    |
 La política trata de:
   - lo que vemos y de lo que podemos decir al respecto,
   - sobre quién tiene la competencia para ver y la cualidad para decir, 
   - sobre las propiedades de los espacios y los posibles del tiempo.
 
 Las prácticas estéticas son "maneras de hacer" que intervienen en la distribución general
 de las maneras de hacer y en sus relaciones con maneras de ser y formas de visibilidad.
 
 La proscripción platónica de los poetas se funda en la imposibilidad de hacer dos cosas al mismo tiempo.
 
 Platón desprende dos grandes forma de existencia y de efectividad sensible
 a la palabra: 
     - el teatro y la escritura.
  |
  |_ Estas se revelan comprometidas con un cierto régimen de la política, 
     un régimen de indeterminación de identidades, de deslegitimación
     de posiciones de palabra, de desregulación de repartos 
     del espacio y tiempo
  |
  |
 La democracia es este régimen estético:
   - el de la asamblea de artesanos, 
   - de leyes escritas intangibles, 
   - de la institucion teatral.
 
 Platón opone la de la coreografía
 
 (Platon) Practicas de la palabra
 - Está la superficie de signos mudos; como pinturas.                            (superficie de signos pintados)
 + Está el espacio del movimiento de los cuerpos que se divide en dos 
   modelos antagónicos:
     - simulacros de la escena, ofrecido a las identificaciones del público      (teatro)
     - el movimiento auténtico, el movimiento propio de los cuerpos comunitarios (coro danzante)
 |_______________________________________________________________________________________________________|
                                        |
                                        |
                       - Tres formas de reparto de lo sensible.
                       - Tres maneras, cuyas prácticas de la palabra
                         y del cuerpo proponen figuras de comunidad.
 |
 |_ estas obras o performances hacen política
 
 Madame Bovary fueron percibidas como "la democracia en la literatura", 
 a pesar de la postura aristocrática y el conformismo político
 de Flaubert.
   |_ Demócrata (según sus adversarios) por pintar en lugar de instruír.
 
 La indiferencia con la que trata aquella literatura es la igualdad misma 
 de todo lo que adviene sobre una página de escritura, disponible 
 para toda mirada.
   |
   |_ Igualdad que destruye las jerarquías de la representación
      e instituye la comunidad de los lectores como
      comunidad sin legitimidad
        |
        |_ Comunidad dibujada por la sola circulación aleatoria de la letra
 
 Politicidad sensible atribuida de entrada a grandes formas de reparto estético
   |
   |_ Esas "políticas" siguen su propia lógica y vuelven
      a proponer sus servicios en épocas y en contextos
      muy diferentes
 
 Una "superficie" no es simplemente una composición geométrica de líneas.
     |_ Es una forma de reparto de lo sensible
     |
     |_ Escritura y pintura eran para Platón superficies
        equivalentes de signos mudos, privados del soplo
        que anima y transporta la palabra viviente
     |
     |_ Lo plano no se opone a lo "profundo"
        sino a lo "vivo".
                   | 
                   |_ Es al acto de palabra "viva", conducida 
                      por el locutor hacia el destinatario adecuado
                      que se opone la superficie muda de los
                      signos pintados
     |
     |__ Lo "plano" de la superficie de los signos pintados interviene
         al mismo tiempo como principio de revolución "formal" de 
         un arte y principio de re-partición político de la
         experiencia común.
     |
     |__ La política se juega ahí como relación entre la escena
         y la sala, significación del cuerpo del actor, juegos
         de la proximidad o de la distancia.
 
 (RS) Intervenciones políticas de artistas pensadas a partir
      del recorte sensible común.
 
 Las artes no prestan nunca, a las empresas de dominación o
 de la emancipación, más de lo que ellas puede prestarles
  |
  |_ Es decir, lo que tienen en común con ellas:
               - posiciones y movimientos de cuerpos
               - funciones de la palabra
               - reparticiones de lo visible y de lo invisible
  |
  |_ Y la autonomía de la que ellas pueden gozar o la 
     subversion que ellas pueden atribuirse, 
     descansan sobre la misma base.
__1_reparto

: << __2_regimenes
 -------------------------------------------------
 
  2. De los regímenes del arte y del escaso
     interés de la noción de modernidad.
 
 -------------------------------------------------
 
 (M) La noción de modernidad estética recubre la singularidad
     de un cierto régimen particular de las artes.
       |
       |_  Es decir, de un tipo específico de vínculo
           entre modos de producción de obras o de prácticas, 
            formas de visibilidad de esas prácticas, 
            y modos de conceptualización
            de unas y otras.
 
 Respecto al arte, se pueden distingir tres grandes regímenes
 de identifícación:
 
 1. (Régimen) Régimen ético de las imágenes:
              El arte no es identificado tal cual:
                  |_ Se encuentra subsumido por la pregunta
                     por las imágenes
 
               Las imágenes (estos seres), son sometidos a una doble
               interrogación:
               - La de su origen:
                          |_ Y por lo tanto por su contenido de verdad.
 
               - La de su destino: 
                          |_ Los usos a los cuáles sirven y los efectos
                             que ellas inducen.
 
               El arte para Platón no existe, sino sólo tres maneras
               de hacer.
                |
                |_ Entre ellas traza las líneas del reparto:
                       - Artes verdaderas
                           |
                           |_ Saberes fundados en la imitación
                              de un modelo con fines definidos
 
                       - Simulacros de arte que imitan simples apariencias
                           |
                           |__ Estas imitaciones, diferenciadas por su origen, 
                               lo son además por su función:
                                                    ___|
                                                   |
                                   Por la manera en que las imágenes del poema 
                                   dan a los niños y a los espectadores ciudadanos
                                   cierta educación.
                                                   |
                                   Y se inscriben en el reparto de ocupaciones
                                   de la polis.
                |
                |
                |_ En este régimen se trata de saber en qué medida 
                   la manera de ser de las imágenes concierne al ethos,
                   la manera de ser de ser de individuos y de colectividades
                |
                |_ Esta cuestión impide al "arte" individualizarse como tal.
    |
    |_ De este (régimen ético) se desprende
                |
 2. (Régimen) Régimen poético -o representativo- de las artes:
                |
                |_ Este identifica el hecho del arte (de las artes)
                   en la pareja poiesis/mimesis
                   |
                   |_ El principio mimético
                        |_ no es, en su fondo, un principio pragmático que diga que el
                           arte deba deba hacer copias que asemejen a sus modelos.
                        |
                        |_ es un principio pragmático que aisla, en el dominio
                           general de las artes (de las maneras de hacer),
                           ciertas artes particulares que ejecutan cosas específicas,
                           a saber, imitaciones.
                                        |
                           Estas imitaciones se sustraen a la vez a:
                                - la verificación ordinaria de los productos de las artes
                                  por su uso,
                                - la legislación de la verdad sobre los discursos y las imágenes
                |
                |_ Es "poético" en el sentido de que identifica a las artes -lo que la 
                       época clásica llamará "bellas artes"- al interior de uan clasificación de las
                       maneras de hacer, y define por consiguiente maneras de hacer bien y de 
                       apreciar las imitaciones.
                |
                |_ Es "representativo" en tanto que es la noción de representación
                       o de "mímesis" la que organiza estas maneras de "hacer", "ver" y "juzgar".
                                |
                                |_ Pero la |"mímesis"| no es la ley que somete las artes
                                   a las semejanzas.
                                    |
                                    |_ Es el pliegue en la distribución:
                                         - de maneras de hacer
                                         - de ocupaciones sociales que hacen visibles a las
                                           artes
                                    |
                                    |_ No es un procedimiento del arte, 
                                       sino un régimen de visibilidad de las artes
                |
                |_ Un régimen de visibilidad de las artes,
                    - es lo que las autonomiza,
                    - también lo que articula esta autonomía 
                      con un orden general de maneras de hacer y de ocupaciones 
                |
                |_ La lógica representativa entra en una relación
                   de analogía global con una jerarquía global de
                   ocupaciones políticas y sociales:
                     |
                     |_ El primado representativo de 
                            la acción sobre los caracteres
                            o de la narración sobre la descripción,
                            la jerarquía de géneros según la dignidad de sus temas,
                            y el primado mismo del arte de la palabra,
                            de la palabra en acto,
                        entran en analogía con toda una visión jerárquica de la comunidad.

 3. (Régimen) Régimen estético de las artes:
                |
                |_ Estético, 
                    porque la definición del arte 
                    ya no se hace por una distinción en el seno de las maneras de hacer,
                    sino por la distinción de un modo de ser sensible
                    propio de los productos del arte.
                |
                |_ La palabra estética remite propiamente al modo de ser específico
                    de lo que pertenece al arte, al modo de ser de sus objetos.
                |
                |_ En este régimen las cosas del arte son identificadas por su pertenencia
                   a un régimen específico de lo sensible:
                     |
                     |_ Este sensible,
                            desligado de sus conexiones ordinarias, 
                         es habitado por una potencia heterogénea,
                         la potencia de un pensamiento que se ha vuelto extranjero a si mismo:
                            |
                            |_ Producto idéntico al no-producto,
                                saber transformado en no-saber,
                                logos idéntico a un pathos,
                                intención de lo inintencional.
                |
                |_ El régimen estético de las artes es el que identifica propiamente al arte en singular
                   y desvincula este arte de toda regla específica, 
                    de toda jerarquía de los temas, los géneros y las artes.
                    |
                    |_ Pero lo hace de modo que hace añicos la barrera mimética 
                       que distinguía las maneras de hacer del arte 
                       respecto de las otras maneras de hacer, 
                        y que separaba sus reglas del ámbito de las ocupaciones sociales.
                         |
                         |_ Afirma la absoluta singularidad del arte y 
                         |  destruye al mismo tiempo todo criterio pragmático de dicha singularidad
                         |  
                         |_ Inaugura al mismo tiempo la autonomía del arte y 
                             la identidad de sus formas con aquellas mediante las cuales 
                             la propia vida se da forma.
                |
                |_ (R)(M) El régimen estético de las artes es el nombre verdadero 
                    de aquello que designa la denominación confusa de modernidad.
                        |
                        |_ La “modernidad” en sus distintas versiones 
                            es el concepto que se aplica para ocultar 
                            la especificidad de este régimen de las artes 
                            y el sentido mismo de la especificidad de los regímenes del arte.
                        |
                        |_ Traza, para exaltarla o para deplorarla, 
                            una sencilla línea de paso o de ruptura entre lo antiguo y lo moderno, 
                            lo representativo y lo no representativo o lo antirrepresentativo.
                |
                |_ El régimen estético de las artes es en primer lugar 
                    un régimen nuevo de la relación con lo antiguo.
                |
                |_ Los modernistas hablan del fin del arte como identificación con la vida de la comunidad, 
                    que es tributaria de la relectura schilleriana y romántica del arte griego 
                    como modo de vida de una comunidad —a la vez que se comunica, por otra parte, 
                    con las nuevas maneras de los inventores publicitarios que, a su vez, 
                    no proponen ninguna revolución, sino solamente una nueva manera de vivir entre las palabras, 
                    las imágenes y las mercancías.
                |
                |_ La idea de modernidad es un concepto equívoco que pretende ser un corte en la configuración 
                    compleja del régimen estético de las artes, 
                    seleccionar las formas de ruptura, 
                        los gestos iconoclastas, etcétera, 
                    separándolos de su contexto:
                                        |
                                        |_ La reproducción generalizada, 
                                            la interpretación, 
                                            la historia, 
                                            el museo, 
                                            el patrimonio, etc.
                     |
                     |_ Pretende que exista un sentido único, 
                        mientras que la temporalidad propia del régimen estético de las artes 
                            es la de una copresencia de temporalidades heterogéneas.
                     |
                     |_ (M) La noción de modernidad parece así inventada a toda prisa 
                        para interferir en la comprensión de las transformaciones del arte 
                        y de sus relaciones con las otras esferas de la experiencia colectiva.
                         |
                         |_ Dos grandes formas de esta interferencia:
                             |
                             |_ La primera busca una modernidad simplemente identificada con la autonomía del arte, 
                                   una revolución “antimimética” del arte idéntica a la conquista de la forma pura, 
                                   al fin puesta al desnudo, del arte.
                                    |
                                    |_ Todo arte afirmaría entonces la pura potencia del arte en la exploración 
                                        de los poderes propios de su medio específico.
                                        |
                                        |_ La modernidad poética o literaria sería la exploración de los poderes de un lenguaje 
                                        |   separado de sus usos comunicacionales. 
                                        |
                                        |_ La modernidad pictórica sería el retorno de la pintura a su propio medio: 
                                        |    - el pigmento coloreado y la superficie bidimensional
                                        |
                                        |_ La modernidad musical se identificaría con el lenguaje de doce sonidos, 
                                               despojado de toda analogía con el lenguaje expresivo.

                                   Y estas modernidades específicas estarían en relación de analogía a distancia 
                                   con una modernidad política, 
                                   susceptible de identificarse, 
                                       según las épocas,
                                   con la radicalidad revolucionaria 
                                   o con la modernidad sobria y desencantada del buen gobierno republicano.
                             |       
                             |_ La segunda forma es el "modernitarismo":
                                  |
                                  |_ La |identificación| de las formas del régimen estético de las artes 
                                       con las formas de cumplimento de una tarea 
                                           o de un destino propio de la modernidad.
                                               |
                                               |
                                  En la base de esta identificación hay una interpretación específica
                                  de la contradicción matricial de la “forma” estética.
                                    |
                                  A lo que se da valor es a la determinación del arte como forma 
                                  y autoformación de la vida.
                          |
                          |_ La modernidad se convirtió en una especie de destino fatal 
                                basado en un olvido fundamental: 
                                    - esencia heideggeriana de la técnica, 
                                      corte revolucionario de la cabeza del rey y de la tradición humana, 
                                      y finalmente pecado original de la criatura humana, 
                                        olvidadiza de su deuda hacia el Otro y de su sumisión a los poderes heterogéneos de lo sensible.
                     |
                     |_ El posmodernismo, en cierto sentido, 
                          ha sido el nombre con el que ciertos artistas y pensadores tomaron conciencia 
                          de lo que había sido el modernismo:
                            un intento desesperado de crear un “ámbito propio del arte”, 
                            vinculándolo a una teleología final simple de la evolución y ruptura históricas.
                        |
                        |_ El posmodernismo se convirtió así en el gran canto
                             fúnebre de lo irrepresentable/intratable/irredimible, 
                             que denuncia la locura moderna de la idea de una autoemancipación de la humanidad 
                             del hombre y su inevitable e interminable culminación en los campos de exterminio.

                     |
                     |_ (V) La noción de vanguardia definió el tipo de tema correspondiente 
                         a la visión modernista y dispuesto a conectar según dicha visión 
                         lo estético y lo político.
                          |
                          |_ Su éxito tiene menos que ver con la cómoda conexión que propone 
                               entre la idea artística de la novedad 
                               y la idea de la dirección política del movimiento,
                               que con la conexión más secreta que establece entre dos ideas de la “vanguardia”.
                          |
                          |_ Existe la noción topográfica y militar de la fuerza que marcha en cabeza, 
                               que ostenta la inteligencia del movimiento, 
                               resume sus fuerzas, 
                               determina el sentido de la evolución histórica
                               y elige las orientaciones políticas subjetivas.
                          |
                          |_ Si el concepto de vanguardia tiene un sentido en el régimen estético de las artes, 
                               es en este aspecto: 
                                 no en el aspecto de los destacamentos avanzados de la novedad artística, 
                                   sino en el aspecto de la invención de las formas sensibles 
                                   y de los cuadros materiales de una vida futura.
                              |
                              |_ Ahí es donde la vanguardia “estética” ha contribuido a la vanguardia “política”,
                                 o donde ha querido y creído contribuir a ella, 
                                    transformando la política en programa total de vida.
                          |  
                          |_ La historia de las relaciones entre partidos y movimientos estéticos es en primer lugar
                             la historia de una confusión, 
                                a veces complacientemente mantenida, 
                                otras veces violentamente denunciada, 
                             entre esas dos ideas de vanguardia, 
                             que son de hecho dos ideas distintas de la subjetividad política: 
                                la idea archipolítica del partido, es decir, 
                                    la idea de una inteligencia política que resume las condiciones esenciales del cambio, 
                                y la idea metapolítica de la subjetividad política global, 
                                    y la idea de la virtualidad en los modos de experiencia sensibles
                                    e innovadores que anticipan la comunidad futura.
__2_regimenes

: << __3_artes_mecanicas
 -------------------------------------------------
 
  3. Sobre las artes mecánicas y la promoción
     estética y científica de los anónimos.
 
 -------------------------------------------------

 El régimen estético de las artes deshace esta correlación entre tema y modo de representación. 
  |
  |_ Esta revolución se produce primero en la literatura.
 
 Estas formas de anulación o inversión de la oposición de lo alto y lo bajo 
   |_
     - Que una época y una sociedad se lean en los rasgos, 
        los hábitos o los gestos de un individuo cualquiera (Balzac), 
     - Que el alcantarillado sea el elemento revelador de una civilización (Hugo)
     - Que la hija del granjero y la mujer del banquero sean tomadas 
       en la potencia igualitaria del estilo como “manera absoluta de ver las cosas” (Flaubert),
   |
   |_ No sólo son anteriores a los poderes de la reproducción mecánica
  
 En cierto sentido, la revolución técnica se produce después de la revolución estética.
   |
   Pero también la revolución estética es en primer lugar la gloria de lo insignificante 
     |
     |_ que es pictórico y literario antes de ser fotográfico o cinematográfico.
  |  
  |_ Esta revolución pertenece a la ciencia del escritor antes que pertenecer a la del historiador.
      |
      |_ Pasar de los grandes acontecimientos y personajes a la vida de los seres anónimos, 
           encontrar los síntomas de una época, 
           una sociedad o una civilización en los detalles ínfimos de la vida corriente, 
           explicar la superficie a través de las capas subterráneas
           y reconstituir mundos a partir de sus vestigios,
         este programa es literario antes que científico.
      |
      |_ Es la literatura misma la que se constituye como una cierta sintomatología de la sociedad
           y opone esta sintomatología a los gritos y a las ficciones de la escena pública.
 
  La historia erudita ha vuelto a asumir la oposición a la vieja historia de príncipes, 
     batallas y tratados, 
     se ha basado en la crónica de las cortes y en las relaciones diplomáticas.
 
  La historia de los modos de vida de las masas 
    y de los ciclos de la vida material, 
    se ha basado en la lectura y en la interpretación de los “testigos mudos”.
 
  La aparición de las masas en la escena de la historia o en las “nuevas imágenes” 
     Es en primer término la lógica estética de un modo de visibilidad 
     que por una parte revoca las escalas de grandeza de la tradición representativa 
     y por otra parte revoca el modelo oratorio de la palabra 
     en favor de la lectura de los signos existentes sobre los cuerpos de las cosas, 
         los hombres y las sociedades.
   |
  Esto es lo que la historia erudita hereda. 
     Pero se propone separar la condición de su nuevo objeto (la vida de los seres anónimos) 
     respecto de su origen literario y de la política de la literatura en la cual se inscribe. 
       |
       |_ Lo que ella deja de lado —y lo que el cine y la fotografía retoman— 
          es esta lógica que deja aparecer la tradición novelesca ese pensamiento de lo verdadero: 
            |
            |_ Lo corriente se convierte en bello como rastro de lo verdadero. 
                 |
                 |_ Y se convierte en rastro de lo verdadero si la despojamos 
                    de su evidencia para convertirla en un jeroglífico, 
                    una figura mitológica o fantasmagórica. 
                                |
     Esta dimensión fantasmagórica de lo verdadero, 
        que pertenece al régimen estético de las artes, 
        ha jugado un papel esencial en la constitución del paradigma crítico de las ciencias humanas y sociales. 
       |
     La teoría marxista del fetichismo es su testimonio más notorio: 
       |
       |_  Es preciso despojar a la mercancía de su apariencia trivial,
           convertirla en un objeto fantasmagórico para leer en ella la expresión de las contradicciones 
           de una sociedad. 
__3_artes_mecanicas

: << __4_modos_de_ficcion
 -------------------------------------------------
 
  4. Sobre si es preciso concluir que
     la historia es ficción. Modos de la ficción.
 
 -------------------------------------------------

 La separación entre la idea de ficción y la idea de mentira
    define la especificidad del régimen representativo de las artes.

 Fingir no es proponer engaños, 
   es elaborar estructuras inteligibles. 
   |
   |_ La poesía no tiene cuentas que rendir sobre la “verdad” de lo que dice, 
        pues desde un principio se compone no de imágenes o enunciados, 
        sino de ficciones, es decir, ordenaciones entre los actos. 
   |
   |_ La otra consecuencia que extrae de ello Aristóteles es la superioridad de la poesía, 
        que da una lógica causal a una ordenación de acontecimientos, 
        por encima de la historia, 
        condenada a representar los acontecimientos según su desorden empírico. 
   |
   |_ En otras palabras, 
        la clara división entre realidad y ficción es también la imposibilidad 
        de una racionalidad de la historia y de su ciencia.
   |
   |_ La soberanía estética de la literatura no es el reinado de la ficción.
        Es un régimen de indistinción tendencial entre 
         |
         |_ La razón de las ordenaciones descriptivas y narrativas de la ficción,
         |_ Y las correspondientes a la descripción y la interpretación de los 
              fenómenos del mundo histórico y social.
         |
         |_ Lo real debe ser ficcionado para ser pensado.
         |
         |
         |_ La política y el arte construyen “ficciones”,
              reordenamientos materiales de signos e imágenes, 
              de las relaciones entre lo que se ve y lo que se dice, 
              entre lo que se hace y lo que se puede hacer.
    |
    |_ Los enunciados políticos o literarios tienen efecto sobre lo real.
         |
         |_ Definen modos de palabra o de acción, 
              pero también regímenes de intensidad sensible.
         |   
         |_ Trazan planos de lo visible, 
              trayectorias entre lo visible y lo decible, 
              relaciones entre modos del ser, 
              modos del hacer y modos del decir.
         |
         |_ Definen variaciones de las intensidades sensibles, 
              de las percepciones y capacidades de los cuerpos.
         |
         |_ Se apoderan así de los seres humanos corrientes, 
              ahondan distancias, 
              abren derivaciones, 
              modifican las maneras, las velocidades y los trayectos que les permiten adherirse a una condición,
              reaccionan a situaciones, 
              reconocen sus imágenes.
         |
         |_ Reconfiguran el mapa de lo sensible mediante una difuminación de la funcionalidad de los gestos 
              y los ritmos adaptados a los ciclos naturales de la producción, 
              la reproducción y la sumisión.
         |
         |_ El hombre es un animal político porque es un animal literario, 
              que se deja apartar de su destino “natural” por el poder de las palabras.
         | 
         |_ Esta literalidad es al mismo tiempo la condición y el efecto
              de la circulación de los enunciados literarios “propiamente dichos”.
         |
         |_ Pero los enunciados se apoderan de los cuerpos y los apartan de su destino 
              en la medida en que no son cuerpos, 
                en el sentido de organismos, 
              sino casi-cuerpos, 
              bloques de palabras que circulan sin padre legítimo que las acompañe hacia un destinatario autorizado.
               |
              Tampoco producen cuerpos colectivos. 
                Antes bien, introducen líneas de fractura, de desincorporación, 
                en los cuerpos colectivos imaginarios.
               | 
              Ésta ha sido siempre, como es sabido, 
              la obsesión de los gobernantes y de los teóricos del buen gobierno, 
              inquietos por el “desclasamiento” producido por la circulación de la escritura.
               |
              La circulación de estos casi-cuerpos determina modificaciones de la percepción sensible de lo común, 
              de la relación entre lo común de la lengua y la distribución sensible de espacios y ocupaciones.
               |
              Dibujan así comunidades aleatorias que contribuyen a la formación de colectivos de enunciación
              que vuelven a poner en cuestión la distribución de papeles,
              territorios y lenguajes —en suma, 
                de esos sujetos políticos que vuelven a poner en tela de juicio
                la división predeterminada de lo sensible. 
               |
              Pero precisamente un colectivo político no es un organismo o un cuerpo comunitario. 
               | 
              Las vías de la subjetivización política
              no son las de la identificación imaginaria 
              sino las de la desincorporación “literaria”.
         | 
         |_ Las “ficciones” del arte y de la política son en este sentido heterotopías, más que utopías.
__4_modos_de_ficcion

: << __5_arte_y_trabajo
 -------------------------------------------------
 
  4. Sobre el arte y el trabajo. 
     En qué sentido las prácticas del arte son 
     y no son una excepción con respecto 
     a otras prácticas.
 
 -------------------------------------------------

 (FR) En la noción de “fábrica de lo sensible” se puede entender 
        en primer término la constitución de un mundo sensible común, 
        de un hábitat común, 
        mediante el trenzado de una pluralidad de actividades humanas.

 Un mundo “común” no es nunca simplemente el ethos, 
   |  la estancia común resultante de la sedimentación de un cierto número de actos entrelazados
   |
   |_ Es siempre una distribución polémica de las maneras de ser 
        y de las “ocupaciones” en un espacio de posibles.
        |
        |_ A partir de ahí se puede plantear la cuestión de la relación 
             entre la “normalidad” del trabajo y la “excepcionalidad” artística.
   |
   |_ En el tercer libro de La República, 
        el mimético es condenado ya no simplemente por la falsedad 
        y por el carácter pernicioso de las imágenes que propone, 
        sino según un principio de división del trabajo que ha servido 
        ya para excluir a los artesanos de todo espacio político común:
          |
          |_ El mimético es, por definición, un ser doble. 
               Hace dos cosas a la vez, 
               mientras que el principio de comunidad bien organizada 
               es que cada uno hace en ella solamente una cosa,
               aquella a la que su “naturaleza” le destina.
      |
      |_ La idea del trabajo no es en principio la de una actividad determinada, 
            de un proceso de transformación material. 
           |
           |_ Es la de una división de lo sensible: una imposibilidad de hacer “otra cosa”, 
                basada en una “ausencia de tiempo”. 
              Esta “imposibilidad” forma parte de la concepción incorporada de la comunidad.
                |
              Plantea el trabajo como la relegación necesaria del trabajador en el espacio-tiempo privado de su ocupación, 
                su exclusión de la participación en lo común.
                | 
                |_ El mimético viene a perturbar esta división: 
                     es un hombre de lo doble,
                     un trabajador que hace dos cosas a la vez.
                |
                |_ Lo más importante es tal vez el correlato: 
                     el mimético da al principio “privado” del trabajo una escena pública. 
                     |
                     |_ Constituye una escena de lo común con aquello que debería determinar 
                          el confinamiento de cada uno en su lugar.
                |
                |_ Es esta redivisión de lo sensible lo que constituye su nocividad, 
                     más aún que el peligro de los simulacros que debilitan las almas. 
                |
                |_ Así pues, la práctica artística no es el exterior del trabajo, 
                     sino su forma de visibilidad desplazada.
                    |
                    |_ La división democrática de lo sensible hace del trabajador un ser doble. 
                         |
                    Saca al artesano de “su” lugar, 
                    el espacio doméstico del trabajo, 
                    y le da el “tiempo” de ser en el espacio de las discusiones públicas 
                    y en la identidad del ciudadano deliberante.

             El principio de ficción que rige el régimen representativo del arte
                es una manera de estabilizar la excepción artística, 
                de asignarla a una teknè, 
                lo cual quiere decir dos cosas:
                 |
                 |_ El arte de las imitaciones es una técnica y no una mentira.
                 |
                 |_ Deja de ser un simulacro, 
                      pero a la vez deja de ser la visibilidad desplazada del trabajo, 
                      como división de lo sensible.

             El régimen estético de las artes trastorna el reparto de los espacios.
                |
                |_ No pone en cuestión simplemente el desdoblamiento mimético 
                     en favor de una inmanencia del pensamiento en la materia sensible.
                |
                |_ Pone también en cuestión el estatuto neutralizado de la teknè, 
                     la idea de la técnica como imposición de una forma de pensamiento
                     a una materia inerte. 
                     |
                     |_ Vuelve a plantear la división de las ocupaciones 
                          que sostiene el reparto de los ámbitos de actividad.
               |
               |_ El estado “estético” de Schiller, 
                    al suspender la oposición entre entendimiento activo 
                    y sensibilidad pasiva, se propone invalidar, 
                        con una idea del arte, 
                    una idea de la sociedad basada en la oposición entre quienes piensan y deciden
                    y quienes se dedican a los trabajos materiales.
            | 
            |_ La producción se afirma como principio de una nueva división de lo sensible, 
                en la medida en que une en un mismo concepto los términos tradicionalmente opuestos
                de la actividad fabricadora y de la visibilidad.
            |    
            |_ Fabricar quería decir habitar el espacio-tiempo privado y oscuro del trabajo alimenticio. 
            |
            |_ Producir une al acto de fabricar el de poner al día,
                 definir una relación nueva entre el hacer y el ver.
            |
            |_ El arte anticipa el trabajo porque realiza su principio: 
                 - la transformación de la materia sensible en presentación en sí de la comunidad.

   Por una parte, el modo estético del pensamiento es mucho más 
     que un pensamiento del arte. 
     |
     |_ Es una idea del pensamiento, ligada a una idea de división de lo sensible.

    Por otra parte, es preciso pensar también en cómo el arte de los artistas
        se encuentra definido a partir de una doble promoción del trabajo: 
          |
          |_ La promoción económica del trabajo como nombre de la actividad humana fundamental, 
               pero también la lucha de los proletarios por sacar el trabajo de su 
               noche —de su exclusión de la visibilidad y de la palabra comunes.

    Cualquiera que sea la especificidad de los circuitos económicos en los que se inserten, 
        las prácticas artísticas no constituyen una “excepción” con respecto a las otras prácticas. 
      |
    Representan y reconfiguran las divisiones de esas actividades.
__5_arte_y_trabajo