Las actividades resueltas se entregan por esta misma vía en un documento anexo.  La fecha de entrega únicamente es tentativa, por favor consulten el Programa y el audio de Presentación.

##############################################################################

Las Actividades deberán presentarse en un documento escrito con una extensión máxima de 200 palabras por cada actividad; es decir, 200 por la 1; 200 por la 2, etc.

1. Lea la Apología de Sócrates y desarrolle los siguientes puntos:
a)  La crítica de Sócrates a los sofistas.

Esta crítica que hace es, a mi parecer, tanto clara —pero indirecta— como sutil. El personaje de Sócrates dice: «Y si habeís oído de alguien decir que yo intento educar a los hombres y que cobro dinero, tampoco es verdad» (p.152). Después, admira la figura de grandes personajes de su época como Gorgias, Pródico e Hipías, aludiendo a la belleza de compartir sus conocimientos al publico general que desee recibir de estos sus enseñanzas, sin necesidad de pagar por estos.

A pie de esa misma página se puede encontrar que el cobrar, este trato económico que hacen los sofistas, y la negativa de Sócrates a recibir dinero, es lo que al parecer los diferencia. Sin embargo, no estoy seguro de que esta sea la última crítica que realiza, puesto que más adelante, Sócrates explica al público que, tratando de encontrar a un sabio, encontró personas que aseguraban serlo, sin embargo, tras una examen de estos, descubría que no lo eran. No habla directamente de los sofistas respecto a estos, sino de aquellos que aseguraban ser sabios (y que quizás lo eran).

b)  ¿Por qué Sócrates es considerado el hombre más sabio de su tiempo? 

Se dice esto debido a que, supuestamente, Querefonte, el amigo de la juventud de Sócrates, le preguntó al Oráculo sobre si había alguien más sabio que este, encontrando como respuesta una negativa por parte del Oráculo.

c)  ¿Cómo entiende Sócrates el ejercicio de la filosofía? 

Como un continuo examen en el que se tiene que estar seguro que uno se preocupa e interesa por la inteligencia y la verdad, por el cuidado del alma.

Al parecer, para Sócrates, su modo de vida es uno de abstinencia de los poderes: familiar, comercial, militar, la opinión pública (al hablar de los «discursos en la asamblea»), político. Sin embargo, esto lo hacía, según dice, para cuidar su vida, ya que, si se encaminaba en estas cosas, por su forma de ser honrada, no podría continuar. En otras palabras, a mi juicio, el personaje Sócrates, como filósofo, tiene esta llamada habla verás, esta habla valiente que, tanto es congruente, pero también pone en peligro su vida.

Por otro lado, también muestra que la vía de este filósofo es, sin apartarse de la gente (que es todo lo contrario), simplemente no asistir a la tribuna del pueblo, tampoco dar consejos a la ciudad. Lo cual vendría a reforzar la idea de que la filosofía se contrapone al poder político.

d)  El cuidado del alma y el autoconocimiento. 

Para el Sócrates de Platón, sería una vergüenza no preocuparse por la verdad, la inteligencia, y el cuidado del alma.

Al auto interrogarse referente a la posibilidad del exilio y mantener silencio durante este, Sócrates menciona que no podría, puesto que, para tener una vida tranquila, se deben de tener conversaciones cada día acerca de la virtud; examinarse a si mismo y a otros. Es aquí cuando se lee su famosa frase: «una vida sin examen no tiene objeto vivirla para el hombre».

e)  ¿En qué sentido Diógenes de Sínope puede ser un socrático? 

En el el texto de los Diálogos, al dar ctrl + f, no aparece ninguna alusión a Diógenes de Sínope. Sin embargo, en el de la "Secta del perro", se menciona que Platón cataloga a Diógenes como un «Sócrates enloquecido». Pero antes, hablando de su profesor, Antístenes, este comparte con Sócrates, el amor por la verdadera riqueza y el desprecio por los placeres. Es esta austeridad la que Diógenes hereda de su profesor, a la par que la auto suficiencia y el enfrentamiento a lo convencional. Sólo que, en lugar de utilizar la ironía, se enfocaba en la parresía.

2. A partir del texto de Platón (Protágoras 317b y ss.) describa las características de la actividad a la que se dedicaba Protágoras.

Protágoras hace alusión a que se dedica a educar a los hombres enseñandoles la buena administración de los bienes familiaraes para que pueda dirigir óptimamente su casa; también, acerca de los asuntos políticos para que el pueda ser el más capaz, tanto en el obrar como en el decir. Es decir, me parece que para los griegos de esta época, ser virtuoso, era «ser el mejor» de todos y ante los demás, y Protágoras enseñaba a tener esta cualidad.

El Socrates de Platón, sintetiza diciendo que la actividad de la que se jacta Protágoras de enseñar, es la de la ciencia política y de hacer a los hombres buenos ciudadanos.

Una característica importante que marca una diferencia a Protágoras de los demás sofistas, es que este se menciona a si mismo no enseña especializaciones técnicas. A la par que es un refugio para los jóvenes que "huyen de estas".

3. A partir de Protágoras (320c y ss.) describa la teoría de Protágoras sobre el origen de la ciencia y de las artes. 

El Protágoras de Platón, describe el mito (amañado, según el pie de página) de Prometeo. En este se muestra a la creación de los animales por parte de los dioses. Sin embargo, todos están especializados y son capaces de defenderse, atacar, o reproducirse a gran escala; pero el hombre no tiene estas cualidades. Protágoras, en el mito, recalca una división entre animales y humanos, donde a lso animales se les dieron sus características, mientras que a los humanos se les separa de estas por un descuido divino, y se les asigna el adjetivo de "desnudo". Debido a esto, Prometeo roba tanto de Hefesto como de Atenea, su sabiduría, además del fuego. A mi manera de verlo, esto muestra la sabiduría técnica, la capacidad de generar herramientas y de transformar gracias al fuego para valerse. Este parentesco les permitió creer y adorar a los dioses. Luego sobrevivir pero sin tener máquinas de guerra que les permitiera valerse en contra de invasiones de fieras.

Protagoras, menciona en el mito que una necesidad fundamental para la humanidad primitiva, era la la ciencia política, con el que, a la vez que derivaban el arte de la guerra para hacerle frente a las fieras, podían convivir de forma armoniosa al estar cohesionados. Zeus envió a Hermes para que les trajera el sentido de la moral y la justicia. Con esta vino el orden y la amistad. Tanto la moral como la justicia, fueron repartidos por igual entre todos los hombres; lo cual se menciona como una diferencia entre los conocimientos específicos. Entonces, el origen de los conocimientos técnicos, es divino y fue repartido desigual a la humanidad; sin embargo, el de la política, la justicia y la moral, fue repartido de manera igual.

#####
https://classroom.google.com/c/MTg0MzMzMTIwNTAx/p/MTg0MzM1MjYzNDky/details
ibdlldt
#####

4. A partir del texto de Carlos García Guál, enumere los nombres de los filósofos cínicos estudiados por él.

En el texto, García Guál, despliega de forma detallada a filósofos como: 1. Antistenes, 2. Diógenes de Sínope, 3. Crates; dándoles a cada uno una sección propia medianamente desplegada. Sin embargo, dedica una sección entera, pero igualmente rica, a otros filósofos como: 4. Mónimo, 5. Onesícrito, 6. Metrocles, 7. Hiparquia, 8. Ménipo, 9. Menedemo. 

Como comentario adicional, creo que es una lástima que, por lo que investigué someramente, se hayan perdido los tres libros escritor por Hiparquia.