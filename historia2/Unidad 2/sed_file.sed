#!/bin/sed -rnf
# -r, para utilizar expresiones regulares extendidas
# -n, para no imprimir el pattern space por defecto
# -f, para indicar que este archivo sea tomado como un script de sed

h  # Copiamos lo que se encuentra en pattern space a hold space

s/([^;]*?;)([[:digit:]]{4})(;.*$)/\2/  #  Lo que se encuentra en el pattern space le dejamos,
									   #+ únicamente los cuatro números en el segundo campo,
									   #+ dejándo únicamente lo de la forma 1002, 2032, etc.

y/0123456789/1234567890/;  #  Ya teniendo sólo esos cuatro dígitos en el pattern space,
                           #+ transliteramos número por su sucesor. Es decir, hacemos
                           #+ un desplazamiento en uno: el 0 al 1, el 2 al 3, etc.
#|                     |
#|_____________________|
#           |
#           |_____ ¡Esto sigue estando en el pattern space!
#                  Es decir, tenemos (únicamente) números como "2121"
#                  que provienen de otros como "1010".

G  #  Al pattern space se le añade un salto de línea, seguido de lo que está
   #+ el hold space (la cadena inicial que guardamos con el comando "h").

# Hasta aquí tenemos en el pattern space algo de la forma: 
#
#    <numerosaumentados>\n<la cadena inicial>
#
# Donde la cadena inicial es de la forma:
#    <fecha>;<numeros anteriores>;<un dígito>
#
#  Ahora tenemos que operar de tal manera que llevemos los números aumentados
#+ de la parte inicial, a en medio de <fecha> y <un dígito>

s/([[:digit:]]{4})\n([^;]*?;)([[:digit:]]{4})(;.*)/\2\1\4/
# |______________|  |_______||______________||___| |_____|
#         |             |            |         |      |
#         |             |            |         |      |__ Modificamos la salida de la
#         |             |            |         |          forma <fecha><numeros aumentados><un digito>
#         |             |            |         |
#         |             |            |         |__ Atrapamos el dígito en \4
#         |             |            |
#         |             |            |__ Atrapamos los números anteriores en \3
#         |             |
#         |             |
#         |             |__ Atrapamos la fecha en \2
#         |               
#         |__ Atrapamos los números aumentados en \1
# Todo esto sucede en el pattern space.

p  # Imprimimos el pattern space
