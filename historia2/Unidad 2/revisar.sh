#!/bin/bash

export file_encendidas='./encendidas.log'
export file_apagadas='./apagadas.log'

declare -a direcciones=(
	#[START] No existen. 
	#  Tendrían que dar error, además,
	#+ se tendrían que registrar en el archivo
	#+ "apagadas.log"
	120.0.0.1
	120.0.0.2
	120.0.0.3
	#[END] No existen

	#[START] Si exsten. 
	#  Tendrían que resultar corretas, además,
	#+ se tendrían que registrar en el archivo
	#+ "encendidas.log"
	es.stackoverflow.com
	google.com
	google.com.mx
	#[END] Si existen
)

test_ip() {
	declare -l direccion="$1"
	
	if ping "$direccion" -q -c1 -W1 &> /dev/null
	then echo "$direccion" >> "$file_encendidas"
	else echo "$direccion" >> "$file_apagadas"
	fi
}
export -f test_ip

tr " " "\n" <<< "${direcciones[@]}" | xargs -P0 -I {} bash -c 'test_ip {}'


# Sólo para verificar el contenido
head *.log