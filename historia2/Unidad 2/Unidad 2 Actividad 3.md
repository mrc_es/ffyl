Recuerden que las fechas de entrega son tentativas. Por favor consulten el Programa y el audio de Presentación

1. Lea el libro VI de República y explique lo siguiente:
a) Las cuatro formas de conocimiento según Platón. 
b) La noción de dialéctica.
c) Las ideas como fundamento ontológico del conocimiento.
 
2. Lea la alegoría de la caverna (República-VII) y también explique:
a) Las cuatro formas de conocimiento según Platón. 
b) La noción de dialéctica.
c) Las ideas como fundamento ontológico del conocimiento.

La actividad deberá entregarse en un documento escrito (adjuntado a esta tarea) con una extensión máxima de 200 palabras por cada inciso de la pregunta (1), y con extensión máxima de 200 palabras por todos los incisos de la pregunta (2).
########################################

**Alumno**: Elizalde Sanchez Marco Cuauhtli
**Número de cuenta**: 311258334
**Asignatura**: Historia de la filosofía II
**Tarea**: Unidad 2, actividad 3

# Libro VI:

### a) Las cuatro formas de conocimiento según Platón.

Platón hace una distinción entre cuatro formas de conocimiento a partir de cuatro tipos de entidades: imágenes, objetos materiales; algunas ideas y abstracciones de los objetos materiales, Ideas. Esto lo esquematiza en una recta dividida en dos partes desiguales, y subdivididas en la misma proporción de la que provienen, quedando así cuatro secciones.

Las primeras dos, forman parte de las opiniones (doxa); las siguientes, del conocimiento. Me recuerda un tanto al inicio del poema de Parménides, en el cual la diosa le muestra que hay dos vías para obtener algo del exterior.

Esta sucesión de entidades son análogas a un ascenso (como lo estudiado en la tarea pasada en la ascensión erótica), de lo visible hasta lo inteligible; desde las imágenes hasta las Ideas.

Estos cuatro tipos se pueden comprender, cada uno, por una forma de conocimiento: 1. Las imágenes, por las conjeturas, 2. Los objetos materiales, por las creencias, 3. Entidades matemáticas, por el pensamiento (discursivo), 4. Las ideas, por la inteligencia.

En la sección de lo inteligible, el pensamiento se dirige hacía un principio; los supuestos (axiomas) de áreas matemáticas, los dejan como tales y no como principios. En cambio,en lo matemático, se parte de supuestos (axiomas) utilizándolos como principios.

Las otras formas de conocimiento relativas a la opinión, Platón las toma como volubles que van de un lado para otro haciendo parecer sin inteligencia a quien hace uso de estas.

### b) La noción de dialéctica.

Para Platón, la dialéctica es una facultad por la cual la razón aprehende lo inteligible utilizando los supuestos como peldaños hasta el principio de todo. Luego desciende hacia una conclusión. 

Esto lo hace únicamente mediante Ideas y concluyendo estas.

También, para Platón, lo real e inteligible sólo puede ser estudiado por lo que denomina "ciencia dialéctica" mediante la inteligencia. Esto a diferencia de los matemáticos a quienes muestra que hacen uso del pensamiento discursivo que utiliza como principios a los supuestos.

### c) Las ideas como fundamento ontológico del conocimiento.

Las Ideas son pensadas, mas no vistas.

En el último y más importante segmento de la línea, referido a la inteligencia, se efectúa aquel camino con Ideas mismas y por medio de estas.

Para Platón, el pensamiento en el segmento de la inteligencia, (como Anaximandro con su apeirón), todo sale y vuelve de y a las Ideas. Jamás se sirve de nada sensible sino de estas entidades inteligibles que no pueden ser vistas, sino únicamente pensadas. De esta forma, se trata de llegar a principios.

# Libro VII:

### a) Las cuatro formas de conocimiento según Platón.

Al redefinir la dialéctica como una ciencia, Platón define las cuatro formas de conocer como "ciencia" y "pensamiento discursivo" a las primeras dos en el conjunto de lo inteligible; "creencia" y "conjetura" en el conjunto de la opinión.

Me parece que estas cuatro formas del conocimiento son parte del recorrido de ascenso por la caverna.

Esta suprema etapa del conocimiento no se halla en en el proceso de razonamiento, sino en el contacto directo del cerebro, del pensamiento, con la "verdad". Como Guthrie expresa «that highest intuition».

### b) La noción de dialéctica.

La metáfora de la caverna es un ascenso a través de la dialéctica en la que el sujeto prisionero desde pequeño, es desencadenado y haciéndole ver que lo que él creía como real, no eran más que imágenes. Esto después de ver que tan sólo eran objetos como los que usan los titiriteros. Después, al arrastrase para salir por la caverna (que es el conjunto de la opinión), llega al mundo del conocimiento en el que sus ojos aún no están acostumbrados. Sobre este primero ve los objetos a través de sus sombras y reflejos sobre el agua (me parece que este sería el pensamiento discursivo), posteriormente los objetos alumbrados por la luz del Sol. Este recorrido es lo que plantea Platón como la dialéctica.

El pensamiento dialéctico es visto como la culminación de los estudios a tal punto que Platón lo menciona como "ciencia". Mientras a los otros tipos de razonamientos (como el geométrico) los etiqueta bajo el nombre de "pensamiento discursivo". 

Para Platón, la dialéctica es necesaria para gobernar iluminado por la idea del Bien.

### c) Las ideas como fundamento ontológico del conocimiento.

Por lo leído en el libro de Guthrie, la Idea de Bien es una idea solar, que está asociada con el Sol y su luz.

Platón afirma que una vez percibida la Idea del Bien, se ha de concluir que esta es la causa de todas las cosas rectas y bellas, que es productora de la verdad y de la inteligencia. Es a través de esta Idea que se puede obrar con sabiduría tanto en asuntos privados como en públicos, es decir, mantiene una coherencia que permea tanto al interior como al exterior en un sólo movimiento del pensamiento.
