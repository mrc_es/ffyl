Recuerden que las fechas de entrega son tentativas. Por favor consulten el Programa y el audio de Presentación.

1. A partir del diálogo Fedro, desarrolle los puntos siguientes: 
a)  Los cuatro tipos de manías (locuras divinas) y sus características. 
b)  La naturaleza de la realidad del lugar supraceleste. 
c)  La constitución tripartita del alma. 
d)  La relación alma-cuerpo

2. Auxiliándose del texto Platón y la Académica responda qué se entiende por la Academia Antigua o primera Academia.

La actividad deberá entregarse en un documento escrito (adjuntado a esta tarea) con una extensión máxima de 200 palabras por cada inciso de la pregunta (1), y con extensión máxima de 200 palabras para la pregunta (2).

El texto Platón y la Academia no está digitalizado completo, pero pueden responder la pregunta con la parte que tienen disponible.

################################

1. A partir del diálogo Fedro, desarrolle los puntos siguientes: 
a)  Los cuatro tipos de manías (locuras divinas) y sus características. 

Pero resulta que, a través de esa demencia, que por cierto es un don que los dioses otorgan, nos llegan grandes bienes.

Sin embargo, es digno de traer a colación el testimonio de aquellos, entre los hombres de entonces, que plasmaron los nombres y que no pensaron que fuera algo para avergonzarse o una especie de oprobio la manía. De lo contrario, a este arte tan bello, que sirve para proyectarnos hacia el futuro, no lo habrían relacionado con este nombre, llamándolo maniké. Más bien fue porque pensaban que era algo bello, al producirse por aliento divino, por lo que se lo pusieron. Pero los hombres de ahora, que ya no saben lo que es bello le interpolan una /, y lo llamaron mantiké. También dieron el nombre de «oionoistiké», a esa indagación sobre el futuro, que practican, por cierto, gente muy sensata, valiéndose de aves y de otros indicios, y eso, porque, partiendo de la reflexión, aporta* al pensamiento, inteligencia e información.

De la misma manera que la mantiké es más perfecta y más digna que la oiónistiké, como ]o era ya por su nombre mismo y por sus obras, tanto más bello es, según el testimonio de los antiguos, la mama que la sensatez, pues una nos la envían los dioses, y la otra es cosa de los hombres

#################
Pero también, en las grandes plagas y penalidades que sobrevienen inesperadamente a algunas estirpes, por antiguas y confusas culpas 47, esa demencia que aparecía y se hacía voz en los que la necesitaban, constituía una liberación, volcada en súplicas y entrega a los e dioses. Se llegó, así, a purificaciones y ceremonias de iniciación, que daban la salud en el presente y para el futuro a quien por ella era tocado, y se encontró, además, solución, en los auténticamente delirantes y posesos, a los ma- 24Sa les que los atenazaban.


############
El tercer grado de locura y de posesión viene de las Musas, cuando se hacen con un alma tierna e impecable, despertándola y alentándola hacia cantos y toda clase de poesía, que al ensalzar mil hechos de los antiguos, educa a los que han de venir 4*. Aquel, pues, que sin la locura de las musas acude a las puertas de la poesía, persuadido de que, como por arte, va a hacerse un verdadero poeta, lo será imperfecto, y la obra que sea capaz de crear, estando en su sano juicio, quedará eclipsab da por la de los inspirados y posesos 