#!/bin/bash

#   Declaramos que el            Declaramos que el
#   delimitador sea el ";"       separador de salida (OFS)
#          |                     sea ";" para ahorrarnos
#          |                     la escritura de caracteres.
#          |                      |
#          |                      |
awk -F ";" -v OFS=";" '{ 
    split($2, campo2, "")
    $2 = ""
    for ( numero in campo2 )
        $2 = sprintf( "%s%s",
                        $2,
                        (campo2[numero] + 1) % 10 )
    print $0
}' numeros.txt