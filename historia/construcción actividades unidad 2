1. Problemáticas que acarrea la interpretación aristotélica de los presocráticos

Gadamer Aristóteles ha recopilado grandes saberes de filósofos contemporáneos y antecedentes a él mismo. Sin embargo, Gadamer menciona que aquello que recopiló lo hizo únicamente motivado por los problemas de su propia filosofía, no tanto por ser fiel o por una labor histórica propiamente con estos saberes y con estos filósofos.

Aristóteles, motivado únicamente por su propia labor, no prestó mayor atención a la fidelidad o exactitud de cada saber de pensadores antiguos que pasaba por sus manos, sino únicamente se centró en su uso personal. Por este motivo es que no se le puede asignar la virtud de fidelidad, o completamente verdadera, a aquellos conocimientos de los pensadores presocráticos que provengan de sus enseñanzas; ya que uno, al tomarlas, estaría bajo la suposición de que Aristóteles se mantuvo fiel a estas ideas. Por lo tanto, los conocimientos de los pensadores presocráticos que aportan sus escritos, vienen envueltos, encapsulados, con los intereses propios de Aristóteles.

Si la cuestión principal es «descubrir, en el empleo de los conceptos, la facultad cognitiva de los antiguos y su capacidad de representación», la potencia del pensamiento y el pensamiento mismo de los antiguos pueden quedar mermados si se toman como si fuesen como realidades o dichos fieles a aquellas citas o menciones que no serían más que otros pensamientos que los ensamblaron para fines propios.

2. Por qué la medicina es el origen de la ciencia griega.

La medicina es un arte práctico y los médicos eran considerados —por analogía— artesanos (al servicio del pueblo).

Los médicos —independientemente de su marco teórico o suposiciones— tenían la necesidad de observar los síntomas de cada caso individual para discernir sobre qué estaba mal, luego decidir qué se tenía que hacer al respecto.

La medicina griega (¿Representada por los hipocráticos?) se muestra a si misma como un arte cuyo proceso de producción necesita que el médico tome del cuerpo y del medio —a través de la experimentación, y la vigilancia— datos que —a través de un juicio que clasifique la pertinencia de cada uno— le permitan emitir un pronóstico. De esta manera, el mal se convierte en una entidad con un carácter y un curso que puede ser previsto. Aquí, en este proceso, se puede vislumbrar la analogía con el artesano que toma elementos del medio, materia, para construir su pieza. El médico y artesano son seres relacionados por lo práctico.

Dicho de otra manera: en la medicina griega, un problema inicialmente difuso y caótico —que vendría a ser el mal— sería transformado en algo previsible a través de un proceso en el que se involucran la experimentación, observación, captura de datos, y un juicio que se encargaría de procesarlos para al final emitir un pronóstico, una generalización. Respecto a esto último, Cornford menciona que se puede sentir el impulso de construir una ciencia teorética sobre la base de las observaciones particulares. Me atrevería a afirmar que gracias a esta manera de proceder es que un mal puede ser enunciado, y, de esta manera, se puede trazar un recorrido.

Al mismo tiempo, la medicina griega, como el conocimiento ingenieril y artístico, muestran que la estratificación de los saberes es útil para poder utilizarlos como herramientas.

3. Lea las páginas indicadas del texto de Cornford y desarrolle la distinción entre el pensamiento filosófico presocrático y el quehacer científico.

En la Grecia antigua únicamente se consideraba ciencia a la medicina. Además de haber conciencia de una oposición entre el método dogmático de los filósofos y del método empírico del médico. Mientras el filosofó natural recurre a postulados abstractos y toma como referencia un estudio sobre el estado originario de las cosas, el médico tomaba la ruta desde cada caso particular hasta lo universal.

Dicho de otra manera, me gustaría utilizar una cita del libro Mientras Agonizo de William Faulkner: 

«Las palabras ascienden derechas como una tenue línea, ligera e inofensiva, mientras que los hechos se arrastran horriblemente pegados al suelo, de forma y manera que, al poco rato, no hay modo de pisar a un tiempo esas dos líneas, por mucho que uno se espatarre. Y también que pecado, amor y miedo no son sino palabras que quienes ni pecaron, ni amaron, ni temieron jamás utilizan para eso que no tienen ni tendrán, hasta que se olviden de las dichosas palabras. Como Cora, que ni freír un huevo sabía»

En este caso me tomaría el atrevimiento de decir que no considero tan acertado decir que mientras el pensamiento filosófico dogmático de la Grecia antigua tomaba un camino descendente, el médico tomaba uno ascendente. Sino que, el primero es vertical en busca de la forma ideal, de tratar de embonar todo a sus preceptos, mientras que el médico es horizontal en tanto siempre se "arrastre horriblemente pegado al suelo", un saber que se adapta a los relieves. Mientas uno sería óptico, el otro sería háptico.

Menciono esto porque noto que la medicina griega (en lo mencionado sobre Hipócrates) está contacto con la Tierra de una manera increíble, quizás proviene directamente de ella en el sentido de que Hipócrates hace mención de que la medicina tiene sus orígenes en el procesamiento de la biomasa (haciendo una distinción entre lo crudo y lo cocido). Este conocimiento permite ver el nivel de contacto que tiene el origen medicina con la Tierra y el cuerpo; con lo que ingresa al cuerpo proveniente de la Tierra. Esta división entre el lo crudo y lo cocido se captaba en relación con una dieta. La dieta sería una estrategia que pone en contacto aquello que proviene Tierra con el hombre, que pone en contacto "esto" (la dieta) con "aquello" (el paciente); "esto" y "aquello" en términos particulares, propios, concretos.

Por otro lado, la filosofía dogmática está en situada en una dimensión vertical en el sentido de que el camino al que llega al cuerpo humano es a través de múltiples deducciones.

El cuerpo humano estaría atravesado por estos dos saberes, el que lo observa, lo compone, y el que lo deduce.

Por otro lado, mientras la teoría filosófica dogmática contenía una cantidad constante (seleccionando una cantidad numérica específica) de elementos abstractos, no necesariamente perceptibles, como generadores de lo existente; en la medicina hipocrática se toma al cuerpo humano como una unidad —de caracter compositivo— compuesta por una cantidad variable, no limitada (indefinida), de atributos heterogéneos y concretos —recuperados por la práctica empírica puesto que su presencia en el cuerpo es directamente perceptible— que se diferencian entre estos tanto por características propias como por su proporción en el cuerpo. Mientras una tiene tendencias de rigidez, la otra muestra mayor flexibilidad.

Otra distinción que noto entre la filosofía dogmática y la medicina, es que la segunda se muestra como un poder que decide qué entra y que no entra al cuerpo, además de la proporción que debe de entrar. La medicina se muestra como el arte de las medidas, de las proporciones, de dar a cada paciente lo justo, lo que es de él y sólo de él según su mal y las condiciones en las que se encuentra. Estaría relacionada de alguna manera con la jurisprudencia al tratar casos específicos en lugar de partir de postulados trascendentes. Por otro lado, me atrevo a especular que la filosofía dogmática aplica la ley trascendente que no mira cada caso más que como una derivación de lo que hay "más allá".
